@extends('layouts.app')

@section('content')

    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Welcome {{auth()->guard('admin')->user()->name}}</h2></div>

            <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                {{ session('Carrinho') }} <br />
                {{ session('key') }} <br />
                You are logged in, type Admin!
            </div>
        </div>
    </div>

@endsection
