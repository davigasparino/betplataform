
    <div class="panel panel-default">
        <div class="btn-group-vertical mr-12 btn-block">
            <a class="btn btn-light text-left"
               href="/admin"><i class="fas fa-home mr-2"></i> Home</a>
            <a class="btn btn-light text-left"
               href="/admin/banner"><i class="fa fa-image mr-2"></i> Banner</a>
            <a class="btn btn-light text-left"
               href="/admin/eaglemoss"><i class="fas fa-chess mr-2"></i> Eaglemoss</a>
            <a class="btn btn-light text-left"
               href=""><i class="fas fa-shopping-bag mr-2"></i> Products</a>
            <a class="btn btn-light text-left"
               href="/admin/msena"><i class="fas fa-shopping-bag mr-2"></i> Msena</a>
        </div>
    </div>
