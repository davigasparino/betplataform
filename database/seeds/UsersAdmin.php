<?php

use Illuminate\Database\Seeder;

class UsersAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('admin')->insert([
            'name' => 'Sr. Administrador',
            'email' => 'admin@betPlataform.com',
            'password' => Hash::make('123456'),
        ]);
    }
}

