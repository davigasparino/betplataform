<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id');
            $table->integer('user_id');
            $table->dateTime('start_at');
            $table->dateTime('subscription_until_at');
            $table->dateTime('finish_at');
            $table->decimal('minimum_bid', 8,2);
            $table->string('title', 100);
            $table->mediumText('description');
            $table->string('type',45);
            $table->string('status', 45);
            $table->mediumText('rules');
            $table->tinyInteger('virtual');
            $table->integer('category_id');
            $table->dateTime('deleted_at');
            $table->decimal('dealer_rate', 8, 2);
            $table->decimal('currently_pot', 8, 2);
            $table->integer('priority_int');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bets');
    }
}
