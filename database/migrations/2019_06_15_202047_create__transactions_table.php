<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->decimal('amount', 8, 2);
            $table->integer('original_currency_id');
            $table->integer('currency_rate_id');
            $table->decimal('original_amount', 8, 2);
            $table->dateTime('created_at');
            $table->string('status', 45);
            $table->dateTime('approved_at');
            $table->string('transactionscol', 45);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
