<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'admin'], function(){
    Route::group(['middleware' => 'auth:admin'], function(){
        Route::get('/admin', 'AdminController@index');
    });

    Route::get('/admin/login', 'AdminController@login');
    Route::get('/admin/logout', 'AdminController@logout');
    Route::post('/admin/login', 'AdminController@postLogin');

});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
